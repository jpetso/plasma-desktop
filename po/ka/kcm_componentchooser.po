# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-05 02:15+0000\n"
"PO-Revision-Date: 2023-02-03 06:14+0100\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.2.2\n"

#: applicationmodel.cpp:67
#, kde-format
msgid "Other…"
msgstr "სხვა…"

#. i18n: ectx: label, entry (browserApplication), group (General)
#: browser_settings.kcfg:9
#, kde-format
msgid "Default web browser"
msgstr "ნაგულისხმები ბრაუზერი"

#: components/componentchooserarchivemanager.cpp:15
#, kde-format
msgid "Select default archive manager"
msgstr "არჩიეთ ნაგულისხმები არქივების მმართველი"

#: components/componentchooserbrowser.cpp:15
#, kde-format
msgid "Select default browser"
msgstr "აირჩიეთ ნაგულისხმები ბრაუზერი"

#: components/componentchooseremail.cpp:20
#, kde-format
msgid "Select default e-mail client"
msgstr "აირჩიეთ ელფოსტის ნაგულისხმები კლიენტი"

#: components/componentchooserfilemanager.cpp:15
#, kde-format
msgid "Select default file manager"
msgstr "აირჩიეთ ფაილების ნაგულისხმები მმართველი"

#: components/componentchoosergeo.cpp:13
#, kde-format
msgid "Select default map"
msgstr "აირჩიეთ ნაგულისხმები რუკა"

#: components/componentchooserimageviewer.cpp:15
#, kde-format
msgid "Select default image viewer"
msgstr "აირჩიეთ ნაგულისხმები სურათების გამხსნელი"

#: components/componentchoosermusicplayer.cpp:15
#, kde-format
msgid "Select default music player"
msgstr "აირჩიეთ ნაგულისხმები მუსიკის დამკვრელი"

#: components/componentchooserpdfviewer.cpp:11
#, kde-format
msgid "Select default PDF viewer"
msgstr "აირჩიეთ ნაგულისხმები PDF-ების გამხსნელი"

#: components/componentchoosertel.cpp:17
#, kde-format
msgid "Select default dialer application"
msgstr "აირჩიეთ ნაგულიხმები დამრეკი აპლიკაცია"

#: components/componentchooserterminal.cpp:25
#, kde-format
msgid "Select default terminal emulator"
msgstr "აირჩიეთ ტერმინალის ნაგულისხმები ემულატორი"

#: components/componentchoosertexteditor.cpp:15
#, kde-format
msgid "Select default text editor"
msgstr "აირჩიეთ ნაგულისხმები ტექსტური რედაქტორი"

#: components/componentchooservideoplayer.cpp:15
#, kde-format
msgid "Select default video player"
msgstr "აირჩიეთ ნაგულისხმები ვიდეოს დამკვრელი"

#: ui/ComponentOverlay.qml:22
#, kde-format
msgid "Details"
msgstr "დეტალები"

#: ui/ComponentOverlay.qml:28
#, kde-format
msgid ""
"This application does not advertise support for the following file types:"
msgstr ""
"ამ აპლიკაციას არ აქვს მითითებული, რომ აქვს ფაილის შემდეგი ტიპების მხარდაჭერა:"

#: ui/ComponentOverlay.qml:42
#, kde-format
msgctxt "@action:button"
msgid "Force Open Anyway"
msgstr "ძალით, მაინც გახსნა"

#: ui/ComponentOverlay.qml:50
#, kde-format
msgid ""
"The following file types are still associated with a different application:"
msgstr "შემდეგი ფაილის ტიპები ჯერ კიდევ სხვა აპლიკაციაზეა მიბმული:"

#: ui/ComponentOverlay.qml:59
#, kde-format
msgctxt "@label %1 is a MIME type and %2 is an application name"
msgid "%1 associated with %2"
msgstr "%1 ასოცირებულია %2-სთან"

#: ui/ComponentOverlay.qml:65
#, kde-format
msgctxt "@action:button %1 is an application name"
msgid "Re-assign-all to %1"
msgstr "ყველას %1-ზე მინიჭება"

#: ui/ComponentOverlay.qml:73
#, kde-format
msgid "Change file type association manually"
msgstr "ფაილის ტიპის ასოცირების ხელით შეცვლა"

#: ui/main.qml:17
#, kde-format
msgid ""
"’%1’ seems to not support the following mimetypes associated with this kind "
"of application: %2"
msgstr ""
"როგორც ჩანს, '%1' -ს შემდეგი MIME ტიპების მხარდაჭერა არ გააჩნია ამ "
"აპლიკაციით: %2"

#: ui/main.qml:45
#, kde-format
msgctxt "Internet related application’s category’s name"
msgid "Internet"
msgstr "ინტერნეტი"

#: ui/main.qml:49
#, kde-format
msgid "Web browser:"
msgstr "ვებ-ბრაუზერი:"

#: ui/main.qml:71
#, kde-format
msgid "Email client:"
msgstr "ელფოსტის კლიენტი:"

#: ui/main.qml:93
#, kde-format
msgctxt "Default phone app"
msgid "Dialer:"
msgstr "დამრეკი:"

#: ui/main.qml:115
#, kde-format
msgctxt "Multimedia related application’s category’s name"
msgid "Multimedia"
msgstr "მულტიმედია"

#: ui/main.qml:120
#, kde-format
msgid "Image viewer:"
msgstr "სურათების გამხსნელი:"

#: ui/main.qml:144
#, kde-format
msgid "Music player:"
msgstr "მუსკის დამკვრელი:"

#: ui/main.qml:167
#, kde-format
msgid "Video player:"
msgstr "ვიდეო დამკვრელი:"

#: ui/main.qml:189
#, kde-format
msgctxt "Documents related application’s category’s name"
msgid "Documents"
msgstr "დოკუმენტები"

#: ui/main.qml:194
#, kde-format
msgid "Text editor:"
msgstr "ტექსტური რედაქტორი:"

#: ui/main.qml:216
#, kde-format
msgid "PDF viewer:"
msgstr "PDF-ების გამხსნელი:"

#: ui/main.qml:238
#, kde-format
msgctxt "Utilities related application’s category’s name"
msgid "Utilities"
msgstr "ხელსაწყოები"

#: ui/main.qml:243
#, kde-format
msgid "File manager:"
msgstr "ფაილების მმართველი:"

#: ui/main.qml:265
#, kde-format
msgid "Terminal emulator:"
msgstr "ტერმინალის ემულატორი:"

#: ui/main.qml:278
#, kde-format
msgid "Archive manager:"
msgstr "არქივების მმართველი:"

#: ui/main.qml:300
#, kde-format
msgctxt "Map related application’s category’s name"
msgid "Map:"
msgstr "რუკა:"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Temuri Doghonadze"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "Temuri.doghonadze@gmail.com"

#~ msgid "Component Chooser"
#~ msgstr "კომპონენტის ამრჩევი"
