# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Shinjo Park <kde@peremen.name>, 2014, 2015, 2019, 2020, 2021, 2023.
# JungHee Lee <daemul72@gmail.com>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-08 02:24+0000\n"
"PO-Revision-Date: 2023-03-01 00:37+0100\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#: contents/ui/ToolBoxContent.qml:265
#, kde-format
msgid "Choose Global Theme…"
msgstr "전역 테마 선택…"

#: contents/ui/ToolBoxContent.qml:272
#, kde-format
msgid "Configure Display Settings…"
msgstr "디스플레이 설정..."

#: contents/ui/ToolBoxContent.qml:293
#, kde-format
msgctxt "@action:button"
msgid "More"
msgstr "더 보기"

#: contents/ui/ToolBoxContent.qml:308
#, kde-format
msgid "Exit Edit Mode"
msgstr "편집 모드 끝내기"

#~ msgid "Finish Customizing Layout"
#~ msgstr "레이아웃 사용자 정의 끝내기"

#~ msgid "Default"
#~ msgstr "기본값"

#~ msgid "Desktop Toolbox"
#~ msgstr "데스크톱 도구 상자"

#~ msgid "Desktop Toolbox — %1 Activity"
#~ msgstr "데스크톱 도구 상자 — %1 활동"

#~ msgid "Lock Screen"
#~ msgstr "화면 잠그기"

#~ msgid "Leave"
#~ msgstr "떠나기"
