# Fuminobu TAKEYAMA <ftake@geeko.jp>, 2015.
# Tomohiro Hyakutake <tomhioo@outlook.jp>, 2019.
# Ryuichi Yamada <ryuichi_ya220@outlook.jp>, 2022, 2023.
# Fumiaki Okushi <fumiaki.okushi@gmail.com>, 2015, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma_shell_org.kde.desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-20 02:04+0000\n"
"PO-Revision-Date: 2023-02-22 23:19+0900\n"
"Last-Translator: Ryuichi Yamada <ryuichi_ya220@outlook.jp>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Lokalize 22.12.2\n"

#: contents/activitymanager/ActivityItem.qml:209
msgid "Currently being used"
msgstr "現在使用中"

#: contents/activitymanager/ActivityItem.qml:249
msgid ""
"Move to\n"
"this activity"
msgstr ""
"このアクティビティ\n"
"に移動"

#: contents/activitymanager/ActivityItem.qml:279
msgid ""
"Show also\n"
"in this activity"
msgstr ""
"このアクティビティ\n"
"にも表示"

#: contents/activitymanager/ActivityItem.qml:341
msgid "Configure"
msgstr "設定"

#: contents/activitymanager/ActivityItem.qml:360
msgid "Stop activity"
msgstr "アクティビティを停止"

#: contents/activitymanager/ActivityList.qml:143
msgid "Stopped activities:"
msgstr "停止したアクティビティ:"

#: contents/activitymanager/ActivityManager.qml:127
msgid "Create activity…"
msgstr "アクティビティを作成..."

#: contents/activitymanager/Heading.qml:59
msgid "Activities"
msgstr "アクティビティ"

#: contents/activitymanager/StoppedActivityItem.qml:138
msgid "Configure activity"
msgstr "アクティビティを設定"

#: contents/activitymanager/StoppedActivityItem.qml:155
msgid "Delete"
msgstr "削除"

#: contents/applet/AppletError.qml:123
msgid "Sorry! There was an error loading %1."
msgstr "すみません。%1 の読み込み中にエラーが発生しました。"

#: contents/applet/AppletError.qml:161
msgid "Copy to Clipboard"
msgstr "クリップボードにコピー"

#: contents/applet/AppletError.qml:184
msgid "View Error Details…"
msgstr "エラーの詳細を表示..."

#: contents/applet/CompactApplet.qml:71
msgid "Open %1"
msgstr "%1 を開く"

#: contents/configuration/AboutPlugin.qml:19
#: contents/configuration/AppletConfiguration.qml:245
msgid "About"
msgstr "情報"

#: contents/configuration/AboutPlugin.qml:47
msgid "Send an email to %1"
msgstr "%1 にメールを送る"

#: contents/configuration/AboutPlugin.qml:61
msgctxt "@info:tooltip %1 url"
msgid "Open website %1"
msgstr "ウェブサイト %1 を開く"

#: contents/configuration/AboutPlugin.qml:125
msgid "Copyright"
msgstr "著作権について"

#: contents/configuration/AboutPlugin.qml:145 contents/explorer/Tooltip.qml:93
msgid "License:"
msgstr "ライセンス:"

#: contents/configuration/AboutPlugin.qml:148
msgctxt "@info:whatsthis"
msgid "View license text"
msgstr "ライセンスのテキストを表示"

#: contents/configuration/AboutPlugin.qml:160
msgid "Authors"
msgstr "作者"

#: contents/configuration/AboutPlugin.qml:170
msgid "Credits"
msgstr "クレジット"

#: contents/configuration/AboutPlugin.qml:181
msgid "Translators"
msgstr "翻訳者"

#: contents/configuration/AboutPlugin.qml:195
msgid "Report a Bug…"
msgstr "バグを報告..."

#: contents/configuration/AppletConfiguration.qml:56
msgid "Keyboard Shortcuts"
msgstr "キーボードショートカット"

#: contents/configuration/AppletConfiguration.qml:293
msgid "Apply Settings"
msgstr "設定を適用"

#: contents/configuration/AppletConfiguration.qml:294
msgid ""
"The settings of the current module have changed. Do you want to apply the "
"changes or discard them?"
msgstr ""
"現在のモジュールの設定が変更されています。変更を適用しますか？それとも破棄し"
"ますか？"

#: contents/configuration/AppletConfiguration.qml:324
msgid "OK"
msgstr "OK"

#: contents/configuration/AppletConfiguration.qml:332
msgid "Apply"
msgstr "適用"

#: contents/configuration/AppletConfiguration.qml:338
#: contents/explorer/AppletAlternatives.qml:179
msgid "Cancel"
msgstr "キャンセル"

#: contents/configuration/ConfigCategoryDelegate.qml:27
msgid "Open configuration page"
msgstr "設定ページを開く"

#: contents/configuration/ConfigurationContainmentActions.qml:21
msgid "Left-Button"
msgstr "左ボタン"

#: contents/configuration/ConfigurationContainmentActions.qml:22
msgid "Right-Button"
msgstr "右ボタン"

#: contents/configuration/ConfigurationContainmentActions.qml:23
msgid "Middle-Button"
msgstr "中央ボタン"

#: contents/configuration/ConfigurationContainmentActions.qml:24
msgid "Back-Button"
msgstr "戻るボタン"

#: contents/configuration/ConfigurationContainmentActions.qml:25
msgid "Forward-Button"
msgstr "進むボタン"

#: contents/configuration/ConfigurationContainmentActions.qml:27
msgid "Vertical-Scroll"
msgstr "縦スクロール"

#: contents/configuration/ConfigurationContainmentActions.qml:28
msgid "Horizontal-Scroll"
msgstr "横スクロール"

#: contents/configuration/ConfigurationContainmentActions.qml:30
msgid "Shift"
msgstr "Shift"

#: contents/configuration/ConfigurationContainmentActions.qml:31
msgid "Ctrl"
msgstr "Ctrl"

#: contents/configuration/ConfigurationContainmentActions.qml:32
msgid "Alt"
msgstr "Alt"

#: contents/configuration/ConfigurationContainmentActions.qml:33
msgid "Meta"
msgstr "Meta"

#: contents/configuration/ConfigurationContainmentActions.qml:95
msgctxt "Concatenation sign for shortcuts, e.g. Ctrl+Shift"
msgid "+"
msgstr "+"

#: contents/configuration/ConfigurationContainmentActions.qml:167
msgctxt "@title"
msgid "About"
msgstr "情報"

#: contents/configuration/ConfigurationContainmentActions.qml:182
#: contents/configuration/MouseEventInputButton.qml:13
msgid "Add Action"
msgstr "アクションを追加"

#: contents/configuration/ConfigurationContainmentAppearance.qml:78
msgid "Layout changes have been restricted by the system administrator"
msgstr "レイアウトの変更はシステムの管理者によって制限されています"

#: contents/configuration/ConfigurationContainmentAppearance.qml:93
msgid "Layout:"
msgstr "レイアウト:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:107
msgid "Wallpaper type:"
msgstr "壁紙の種類:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:127
msgid "Get New Plugins…"
msgstr "新しいプラグインを入手..."

#: contents/configuration/ConfigurationContainmentAppearance.qml:195
msgid "Layout changes must be applied before other changes can be made"
msgstr "他の変更を行う前にレイアウトの変更を適用してください。"

#: contents/configuration/ConfigurationContainmentAppearance.qml:199
msgid "Apply Now"
msgstr "今すぐ適用"

#: contents/configuration/ConfigurationShortcuts.qml:16
msgid "Shortcuts"
msgstr "ショートカット"

#: contents/configuration/ConfigurationShortcuts.qml:28
msgid "This shortcut will activate the applet as though it had been clicked."
msgstr ""
"ここで設定したショートカットで、アプレットがクリックされたときのようにアク"
"ティブになります。"

#: contents/configuration/ContainmentConfiguration.qml:29
msgid "Wallpaper"
msgstr "壁紙"

#: contents/configuration/ContainmentConfiguration.qml:34
msgid "Mouse Actions"
msgstr "マウスアクション"

#: contents/configuration/MouseEventInputButton.qml:20
msgid "Input Here"
msgstr "ここに入力"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:44
#, fuzzy
#| msgid "Apply Settings"
msgid "Panel Settings"
msgstr "設定を適用"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:50
#, fuzzy
#| msgid "Maximize Panel"
msgctxt "@action:button Make the panel as big as it can be"
msgid "Maximize"
msgstr "パネルを最大化"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:54
msgid "Make this panel as tall as possible"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:55
msgid "Make this panel as wide as possible"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:63
#, fuzzy
#| msgid "Delete"
msgctxt "@action:button Delete the panel"
msgid "Delete"
msgstr "削除"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:66
msgid "Remove this panel; this action is undo-able"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:92
#, fuzzy
#| msgid "Panel Alignment"
msgid "Alignment:"
msgstr "パネルの配置"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:98
msgid "Top"
msgstr "上"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:98
msgid "Left"
msgstr "左"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:99
msgid ""
"Aligns a non-maximized panel to the top; no effect when panel is maximized"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:99
msgid ""
"Aligns a non-maximized panel to the left; no effect when panel is maximized"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:107
msgid "Center"
msgstr "中央"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:108
msgid ""
"Aligns a non-maximized panel to the center; no effect when panel is maximized"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:116
msgid "Bottom"
msgstr "下"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:116
msgid "Right"
msgstr "右"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:117
msgid ""
"Aligns a non-maximized panel to the bottom; no effect when panel is maximized"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:117
msgid ""
"Aligns a non-maximized panel to the right; no effect when panel is maximized"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:150
#, fuzzy
#| msgid "Visibility"
msgid "Visibility:"
msgstr "表示"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:155
msgid "Always Visible"
msgstr "常に表示"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:163
#, fuzzy
#| msgid "Auto Hide"
msgid "Auto-Hide"
msgstr "自動的に隠す"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:164
msgid ""
"Panel is hidden, but reveals itself when the cursor touches the panel's "
"screen edge"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:193
#, fuzzy
#| msgid "Opacity"
msgid "Opacity:"
msgstr "透明度"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:198
#, fuzzy
#| msgid "Opaque"
msgid "Always Opaque"
msgstr "不透明"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:206
msgid "Adaptive"
msgstr "アダプティブ"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:207
msgid ""
"Panel is opaque when any windows are touching it, and translucent at other "
"times"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:216
#, fuzzy
#| msgid "Translucent"
msgid "Always Translucent"
msgstr "透明"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:245
#, fuzzy
#| msgid "Floating Panel"
msgid "Floating:"
msgstr "フローティングパネル"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:250
#, fuzzy
#| msgid "Floating Panel"
msgid "Floating"
msgstr "フローティングパネル"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:251
msgid "Panel visibly floats away from its screen edge"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:260
msgid "Attached"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:261
msgid "Panel is attached to its screen edge"
msgstr ""

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:283
#, fuzzy
#| msgid "Shortcut"
msgid "Focus Shortcut:"
msgstr "ショートカット"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:293
msgid "Press this keyboard shortcut to move focus to the Panel"
msgstr "キーボードショートカットを押してパネルをフォーカス"

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum height."
msgstr "ドラッグして最大の高さを変更"

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum width."
msgstr "ドラッグして最大の幅を変更"

#: contents/configuration/panelconfiguration/Ruler.qml:20
#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Double click to reset."
msgstr "ダブルクリックしてリセット"

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum height."
msgstr "ドラッグして最小の高さを変更"

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum width."
msgstr "ドラッグして最小の幅を変更"

#: contents/configuration/panelconfiguration/Ruler.qml:69
msgid ""
"Drag to change position on this screen edge.\n"
"Double click to reset."
msgstr ""
"ドラッグしてこのスクリーンの端での位置を変更\n"
"ダブルクリックしてリセット"

#: contents/configuration/panelconfiguration/ToolBar.qml:24
msgid "Add Widgets…"
msgstr "ウィジェットを追加..."

#: contents/configuration/panelconfiguration/ToolBar.qml:25
msgid "Add Spacer"
msgstr "スペーサーを追加"

#: contents/configuration/panelconfiguration/ToolBar.qml:26
msgid "More Options…"
msgstr "その他の設定..."

#: contents/configuration/panelconfiguration/ToolBar.qml:222
msgctxt "Minimize the length of this string as much as possible"
msgid "Drag to move"
msgstr "ドラッグして移動"

#: contents/configuration/panelconfiguration/ToolBar.qml:261
msgctxt "@info:tooltip"
msgid "Use arrow keys to move the panel"
msgstr "矢印キーを使用してパネルを移動"

#: contents/configuration/panelconfiguration/ToolBar.qml:282
msgid "Panel width:"
msgstr "パネルの幅:"

#: contents/configuration/panelconfiguration/ToolBar.qml:282
msgid "Panel height:"
msgstr "パネルの高さ:"

#: contents/configuration/panelconfiguration/ToolBar.qml:402
#: contents/configuration/ShellContainmentConfiguration.qml:43
msgid "Close"
msgstr "閉じる"

#: contents/configuration/ShellContainmentConfiguration.qml:19
msgid "Panels and Desktops Management"
msgstr "パネルとデスクトップの管理"

#: contents/configuration/ShellContainmentConfiguration.qml:34
msgid ""
"You can drag Panels and Desktops around to move them to different screens."
msgstr "パネルとデスクトップをドラッグして他のスクリーンに移動できます。"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:178
msgid "Swap with Desktop on Screen %1"
msgstr "スクリーン %1 のデスクトップと入れ替える"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:179
msgid "Move to Screen %1"
msgstr "スクリーン %1 に移動"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:193
#, fuzzy
#| msgid "Remove Panel"
msgid "Remove Desktop"
msgstr "パネルを削除"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:194
msgid "Remove Panel"
msgstr "パネルを削除"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:267
msgid "%1 (primary)"
msgstr "%1 (プライマリ)"

#: contents/explorer/AppletAlternatives.qml:63
msgid "Alternative Widgets"
msgstr "代替ウィジェット"

#: contents/explorer/AppletAlternatives.qml:163
msgid "Switch"
msgstr "切り替え"

#: contents/explorer/AppletDelegate.qml:177
msgid "Undo uninstall"
msgstr "アンインストールを取り消し"

#: contents/explorer/AppletDelegate.qml:178
msgid "Uninstall widget"
msgstr "ウィジェットをアンインストール"

#: contents/explorer/Tooltip.qml:102
msgid "Author:"
msgstr "作者:"

#: contents/explorer/Tooltip.qml:110
msgid "Email:"
msgstr "メールアドレス:"

#: contents/explorer/Tooltip.qml:129
msgid "Uninstall"
msgstr "アンインストール"

#: contents/explorer/WidgetExplorer.qml:129
#: contents/explorer/WidgetExplorer.qml:240
msgid "All Widgets"
msgstr "すべてのウィジェット"

#: contents/explorer/WidgetExplorer.qml:194
msgid "Widgets"
msgstr "ウィジェット"

#: contents/explorer/WidgetExplorer.qml:202
msgid "Get New Widgets…"
msgstr "新しいウィジェットを入手..."

#: contents/explorer/WidgetExplorer.qml:251
msgid "Categories"
msgstr "カテゴリ"

#: contents/explorer/WidgetExplorer.qml:331
msgid "No widgets matched the search terms"
msgstr "検索語に一致するウィジェットはありません"

#: contents/explorer/WidgetExplorer.qml:331
msgid "No widgets available"
msgstr "利用可能なウィジェットはありません"

#, fuzzy
#~| msgid "Windows Go Below"
#~ msgid "Windows Above"
#~ msgstr "ウィンドウはパネルの下に隠れる"

#, fuzzy
#~| msgid "Windows Go Below"
#~ msgid "Windows Below"
#~ msgstr "ウィンドウはパネルの下に隠れる"

#, fuzzy
#~| msgid "Windows Can Cover"
#~ msgid "Windows In Front"
#~ msgstr "ウィンドウはパネルを覆う"
