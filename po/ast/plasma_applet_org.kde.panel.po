# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Enol P. <enolp@softastur.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-14 00:48+0000\n"
"PO-Revision-Date: 2023-05-03 21:46+0200\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: \n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.0\n"

#: contents/ui/ConfigOverlay.qml:260 contents/ui/ConfigOverlay.qml:295
#, kde-format
msgid "Remove"
msgstr ""

#: contents/ui/ConfigOverlay.qml:270
#, kde-format
msgid "Configure…"
msgstr ""

#: contents/ui/ConfigOverlay.qml:280
#, kde-format
msgid "Show Alternatives…"
msgstr ""

#: contents/ui/ConfigOverlay.qml:305
#, kde-format
msgid "Spacer width"
msgstr ""
