# translation of kcmkonq.po to Oriya
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Manoj Kumar Giri <mgiri@redhat.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kcmkonq\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-05 02:15+0000\n"
"PO-Revision-Date: 2009-01-02 11:45+0530\n"
"Last-Translator: Manoj Kumar Giri <mgiri@redhat.com>\n"
"Language-Team: Oriya <oriya-it@googlegroups.com>\n"
"Language: or\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"\n"
"\n"

#: desktoppathssettings.cpp:211
#, kde-format
msgid "Desktop"
msgstr ""

#: desktoppathssettings.cpp:226
#, kde-format
msgid "Documents"
msgstr ""

#: desktoppathssettings.cpp:241
#, kde-format
msgid "Downloads"
msgstr ""

#: desktoppathssettings.cpp:256
#, kde-format
msgid "Music"
msgstr ""

#: desktoppathssettings.cpp:271
#, kde-format
msgid "Pictures"
msgstr ""

#: desktoppathssettings.cpp:286
#, kde-format
msgid "Videos"
msgstr ""

#: desktoppathssettings.cpp:301
#, kde-format
msgid "Public"
msgstr ""

#: desktoppathssettings.cpp:316
#, kde-format
msgid "Templates"
msgstr ""

#: ui/main.qml:23
#, kde-format
msgid "Desktop path:"
msgstr ""

#: ui/main.qml:26
#, kde-format
msgid ""
"This folder contains all the files which you see on your desktop. You can "
"change the location of this folder if you want to, and the contents will "
"move automatically to the new location as well."
msgstr ""

#: ui/main.qml:32
#, kde-format
msgid "Documents path:"
msgstr ""

#: ui/main.qml:35
#, kde-format
msgid ""
"This folder will be used by default to load or save documents from or to."
msgstr ""

#: ui/main.qml:41
#, kde-format
msgid "Downloads path:"
msgstr ""

#: ui/main.qml:44
#, kde-format
msgid "This folder will be used by default to save your downloaded items."
msgstr ""

#: ui/main.qml:50
#, kde-format
msgid "Videos path:"
msgstr ""

#: ui/main.qml:53 ui/main.qml:80
#, kde-format
msgid "This folder will be used by default to load or save movies from or to."
msgstr ""

#: ui/main.qml:59
#, kde-format
msgid "Pictures path:"
msgstr ""

#: ui/main.qml:62
#, kde-format
msgid ""
"This folder will be used by default to load or save pictures from or to."
msgstr ""

#: ui/main.qml:68
#, kde-format
msgid "Music path:"
msgstr ""

#: ui/main.qml:71
#, kde-format
msgid "This folder will be used by default to load or save music from or to."
msgstr ""

#: ui/main.qml:77
#, kde-format
msgid "Public path:"
msgstr ""

#: ui/main.qml:86
#, kde-format
msgid "Templates path:"
msgstr ""

#: ui/main.qml:89
#, kde-format
msgid ""
"This folder will be used by default to load or save templates from or to."
msgstr ""

#: ui/UrlRequester.qml:65
#, kde-format
msgctxt "@action:button"
msgid "Choose new location"
msgstr ""
