# Translation of kcm_desktoppaths to Norwegian Nynorsk
#
# Gaute Hvoslef Kvalnes <gaute@verdsveven.com>, 1999, 2000, 2002, 2003, 2004, 2005.
# Håvard Korsvoll <korsvoll@skulelinux.no>, 2003.
# Karl Ove Hufthammer <karl@huftis.org>, 2007, 2008, 2010, 2013, 2018, 2020, 2021, 2023.
# Eirik U. Birkeland <eirbir@gmail.com>, 2008, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kcmkonq\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-05 02:15+0000\n"
"PO-Revision-Date: 2023-02-20 21:17+0100\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: desktoppathssettings.cpp:211
#, kde-format
msgid "Desktop"
msgstr "Skrivebord"

#: desktoppathssettings.cpp:226
#, kde-format
msgid "Documents"
msgstr "Dokument"

#: desktoppathssettings.cpp:241
#, kde-format
msgid "Downloads"
msgstr "Nedlastingar"

#: desktoppathssettings.cpp:256
#, kde-format
msgid "Music"
msgstr "Musikk"

#: desktoppathssettings.cpp:271
#, kde-format
msgid "Pictures"
msgstr "Bilete"

#: desktoppathssettings.cpp:286
#, kde-format
msgid "Videos"
msgstr "Videoar"

#: desktoppathssettings.cpp:301
#, kde-format
msgid "Public"
msgstr "Offentleg"

#: desktoppathssettings.cpp:316
#, kde-format
msgid "Templates"
msgstr "Malar"

#: ui/main.qml:23
#, kde-format
msgid "Desktop path:"
msgstr "Mappe for skrivebordet:"

#: ui/main.qml:26
#, kde-format
msgid ""
"This folder contains all the files which you see on your desktop. You can "
"change the location of this folder if you want to, and the contents will "
"move automatically to the new location as well."
msgstr ""
"Denne mappa inneheld alle filene du ser på skrivebordet. Du kan endra "
"plasseringa av mappa, og mappe­innhaldet vert då automatisk flytta til den "
"nye plasseringa."

#: ui/main.qml:32
#, kde-format
msgid "Documents path:"
msgstr "Mappe for dokument:"

#: ui/main.qml:35
#, kde-format
msgid ""
"This folder will be used by default to load or save documents from or to."
msgstr "Dette er mappa som dokument vanlegvis vert lagra til eller lesne frå."

#: ui/main.qml:41
#, kde-format
msgid "Downloads path:"
msgstr "Mappe for nedlastingar:"

#: ui/main.qml:44
#, kde-format
msgid "This folder will be used by default to save your downloaded items."
msgstr "Dette er mappa som nedlasta filer vanlegvis vert lagra i."

#: ui/main.qml:50
#, kde-format
msgid "Videos path:"
msgstr "Mappe for videoar:"

#: ui/main.qml:53 ui/main.qml:80
#, kde-format
msgid "This folder will be used by default to load or save movies from or to."
msgstr ""
"Dette er mappa som filmar og videoklipp vanlegvis vert lagra til eller lesne "
"frå."

#: ui/main.qml:59
#, kde-format
msgid "Pictures path:"
msgstr "Mappe for bilete:"

#: ui/main.qml:62
#, kde-format
msgid ""
"This folder will be used by default to load or save pictures from or to."
msgstr "Dette er mappa som bilete vanlegvis vert lagra til eller lesne frå."

#: ui/main.qml:68
#, kde-format
msgid "Music path:"
msgstr "Mappe for musikk:"

#: ui/main.qml:71
#, kde-format
msgid "This folder will be used by default to load or save music from or to."
msgstr ""
"Dette er mappa som musikkfiler vanlegvis vert lagra til eller lesne frå."

#: ui/main.qml:77
#, kde-format
msgid "Public path:"
msgstr "Offentleg mappe:"

#: ui/main.qml:86
#, kde-format
msgid "Templates path:"
msgstr "Mappe for malar:"

#: ui/main.qml:89
#, kde-format
msgid ""
"This folder will be used by default to load or save templates from or to."
msgstr "Dette er mappa som malar vanlegvis vert lagra til eller lesne frå."

#: ui/UrlRequester.qml:65
#, kde-format
msgctxt "@action:button"
msgid "Choose new location"
msgstr "Vel ny mappe"
