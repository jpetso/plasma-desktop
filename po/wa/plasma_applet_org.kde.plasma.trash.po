# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Jean Cayron <jean.cayron@gmail.com>, 2008, 2009.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_trash\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-18 01:01+0000\n"
"PO-Revision-Date: 2009-05-18 12:00+0200\n"
"Last-Translator: Jean Cayron <jean.cayron@gmail.com>\n"
"Language-Team: Walloon <linux@walon.org>\n"
"Language: wa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 0.3\n"

#: contents/ui/main.qml:100
#, fuzzy, kde-format
#| msgid "&Open"
msgctxt "a verb"
msgid "Open"
msgstr "&Drovi"

#: contents/ui/main.qml:101
#, fuzzy, kde-format
#| msgctxt "The trash is empty. This is not an action, but a state"
#| msgid "Empty"
msgctxt "a verb"
msgid "Empty"
msgstr "Vude"

#: contents/ui/main.qml:107
#, kde-format
msgid "Trash Settings…"
msgstr ""

#: contents/ui/main.qml:158
#, fuzzy, kde-format
#| msgid "Trash"
msgid ""
"Trash\n"
"Empty"
msgstr "Batch"

#: contents/ui/main.qml:158
#, fuzzy, kde-format
#| msgid "One item"
#| msgid_plural "%1 items"
msgid ""
"Trash\n"
"One item"
msgid_plural ""
"Trash\n"
" %1 items"
msgstr[0] "On cayet"
msgstr[1] "%1 cayets"

#: contents/ui/main.qml:167
#, kde-format
msgid "Trash"
msgstr "Batch"

#: contents/ui/main.qml:168
#, fuzzy, kde-format
#| msgctxt "The trash is empty. This is not an action, but a state"
#| msgid "Empty"
msgid "Empty"
msgstr "Vude"

#: contents/ui/main.qml:168
#, fuzzy, kde-format
#| msgid "One item"
#| msgid_plural "%1 items"
msgid "One item"
msgid_plural "%1 items"
msgstr[0] "On cayet"
msgstr[1] "%1 cayets"

#, fuzzy
#~| msgctxt "@action:button"
#~| msgid "Empty Trash"
#~ msgid "Empty Trash"
#~ msgstr "Vudî batch"

#, fuzzy
#~| msgctxt "@info"
#~| msgid "Do you really want to empty the trash? All items will be deleted."
#~ msgid ""
#~ "Do you really want to empty the trash ? All the items will be deleted."
#~ msgstr "Voloz vs vudî l' batch podbon? Tos les cayets seront disfacés."

#~ msgid "&Empty Trashcan"
#~ msgstr "&Vudî batch"

#~ msgid "&Menu"
#~ msgstr "&Dressêye"

#, fuzzy
#~| msgctxt "@action:button"
#~| msgid "Empty Trash"
#~ msgctxt "@title:window"
#~ msgid "Empty Trash"
#~ msgstr "Vudî batch"

#~ msgid "Emptying Trashcan..."
#~ msgstr "Dji vude li batch ås mannestés..."
