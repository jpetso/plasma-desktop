# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Steve Allewell <steve.allewell@gmail.com>, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-20 02:04+0000\n"
"PO-Revision-Date: 2023-05-21 14:52+0100\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.03.70\n"

#: contents/activitymanager/ActivityItem.qml:209
msgid "Currently being used"
msgstr "Currently being used"

#: contents/activitymanager/ActivityItem.qml:249
msgid ""
"Move to\n"
"this activity"
msgstr ""
"Move to\n"
"this activity"

#: contents/activitymanager/ActivityItem.qml:279
msgid ""
"Show also\n"
"in this activity"
msgstr ""
"Show also\n"
"in this activity"

#: contents/activitymanager/ActivityItem.qml:341
msgid "Configure"
msgstr "Configure"

#: contents/activitymanager/ActivityItem.qml:360
msgid "Stop activity"
msgstr "Stop activity"

#: contents/activitymanager/ActivityList.qml:143
msgid "Stopped activities:"
msgstr "Stopped activities:"

#: contents/activitymanager/ActivityManager.qml:127
msgid "Create activity…"
msgstr "Create activity…"

#: contents/activitymanager/Heading.qml:59
msgid "Activities"
msgstr "Activities"

#: contents/activitymanager/StoppedActivityItem.qml:138
msgid "Configure activity"
msgstr "Configure activity"

#: contents/activitymanager/StoppedActivityItem.qml:155
msgid "Delete"
msgstr "Delete"

#: contents/applet/AppletError.qml:123
msgid "Sorry! There was an error loading %1."
msgstr "Sorry! There was an error loading %1."

#: contents/applet/AppletError.qml:161
msgid "Copy to Clipboard"
msgstr "Copy to Clipboard"

#: contents/applet/AppletError.qml:184
msgid "View Error Details…"
msgstr "View Error Details…"

#: contents/applet/CompactApplet.qml:71
msgid "Open %1"
msgstr "Open %1"

#: contents/configuration/AboutPlugin.qml:19
#: contents/configuration/AppletConfiguration.qml:245
msgid "About"
msgstr "About"

#: contents/configuration/AboutPlugin.qml:47
msgid "Send an email to %1"
msgstr "Send an email to %1"

#: contents/configuration/AboutPlugin.qml:61
msgctxt "@info:tooltip %1 url"
msgid "Open website %1"
msgstr "Open website %1"

#: contents/configuration/AboutPlugin.qml:125
msgid "Copyright"
msgstr "Copyright"

#: contents/configuration/AboutPlugin.qml:145 contents/explorer/Tooltip.qml:93
msgid "License:"
msgstr "Licence:"

#: contents/configuration/AboutPlugin.qml:148
msgctxt "@info:whatsthis"
msgid "View license text"
msgstr "View licence text"

#: contents/configuration/AboutPlugin.qml:160
msgid "Authors"
msgstr "Authors"

#: contents/configuration/AboutPlugin.qml:170
msgid "Credits"
msgstr "Credits"

#: contents/configuration/AboutPlugin.qml:181
msgid "Translators"
msgstr "Translators"

#: contents/configuration/AboutPlugin.qml:195
msgid "Report a Bug…"
msgstr "Report a Bug…"

#: contents/configuration/AppletConfiguration.qml:56
msgid "Keyboard Shortcuts"
msgstr "Keyboard Shortcuts"

#: contents/configuration/AppletConfiguration.qml:293
msgid "Apply Settings"
msgstr "Apply Settings"

#: contents/configuration/AppletConfiguration.qml:294
msgid ""
"The settings of the current module have changed. Do you want to apply the "
"changes or discard them?"
msgstr ""
"The settings of the current module have changed. Do you want to apply the "
"changes or discard them?"

#: contents/configuration/AppletConfiguration.qml:324
msgid "OK"
msgstr "OK"

#: contents/configuration/AppletConfiguration.qml:332
msgid "Apply"
msgstr "Apply"

#: contents/configuration/AppletConfiguration.qml:338
#: contents/explorer/AppletAlternatives.qml:179
msgid "Cancel"
msgstr "Cancel"

#: contents/configuration/ConfigCategoryDelegate.qml:27
msgid "Open configuration page"
msgstr "Open configuration page"

#: contents/configuration/ConfigurationContainmentActions.qml:21
msgid "Left-Button"
msgstr "Left-Button"

#: contents/configuration/ConfigurationContainmentActions.qml:22
msgid "Right-Button"
msgstr "Right-Button"

#: contents/configuration/ConfigurationContainmentActions.qml:23
msgid "Middle-Button"
msgstr "Middle-Button"

#: contents/configuration/ConfigurationContainmentActions.qml:24
msgid "Back-Button"
msgstr "Back-Button"

#: contents/configuration/ConfigurationContainmentActions.qml:25
msgid "Forward-Button"
msgstr "Forward-Button"

#: contents/configuration/ConfigurationContainmentActions.qml:27
msgid "Vertical-Scroll"
msgstr "Vertical-Scroll"

#: contents/configuration/ConfigurationContainmentActions.qml:28
msgid "Horizontal-Scroll"
msgstr "Horizontal-Scroll"

#: contents/configuration/ConfigurationContainmentActions.qml:30
msgid "Shift"
msgstr "Shift"

#: contents/configuration/ConfigurationContainmentActions.qml:31
msgid "Ctrl"
msgstr "Ctrl"

#: contents/configuration/ConfigurationContainmentActions.qml:32
msgid "Alt"
msgstr "Alt"

#: contents/configuration/ConfigurationContainmentActions.qml:33
msgid "Meta"
msgstr "Meta"

#: contents/configuration/ConfigurationContainmentActions.qml:95
msgctxt "Concatenation sign for shortcuts, e.g. Ctrl+Shift"
msgid "+"
msgstr "+"

#: contents/configuration/ConfigurationContainmentActions.qml:167
msgctxt "@title"
msgid "About"
msgstr "About"

#: contents/configuration/ConfigurationContainmentActions.qml:182
#: contents/configuration/MouseEventInputButton.qml:13
msgid "Add Action"
msgstr "Add Action"

#: contents/configuration/ConfigurationContainmentAppearance.qml:78
msgid "Layout changes have been restricted by the system administrator"
msgstr "Layout changes have been restricted by the system administrator"

#: contents/configuration/ConfigurationContainmentAppearance.qml:93
msgid "Layout:"
msgstr "Layout:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:107
msgid "Wallpaper type:"
msgstr "Wallpaper type:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:127
msgid "Get New Plugins…"
msgstr "Get New Plugins…"

#: contents/configuration/ConfigurationContainmentAppearance.qml:195
msgid "Layout changes must be applied before other changes can be made"
msgstr "Layout changes must be applied before other changes can be made"

#: contents/configuration/ConfigurationContainmentAppearance.qml:199
msgid "Apply Now"
msgstr "Apply Now"

#: contents/configuration/ConfigurationShortcuts.qml:16
msgid "Shortcuts"
msgstr "Shortcuts"

#: contents/configuration/ConfigurationShortcuts.qml:28
msgid "This shortcut will activate the applet as though it had been clicked."
msgstr "This shortcut will activate the applet as though it had been clicked."

#: contents/configuration/ContainmentConfiguration.qml:29
msgid "Wallpaper"
msgstr "Wallpaper"

#: contents/configuration/ContainmentConfiguration.qml:34
msgid "Mouse Actions"
msgstr "Mouse Actions"

#: contents/configuration/MouseEventInputButton.qml:20
msgid "Input Here"
msgstr "Input Here"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:44
msgid "Panel Settings"
msgstr "Panel Settings"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:50
msgctxt "@action:button Make the panel as big as it can be"
msgid "Maximize"
msgstr "Maximise"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:54
msgid "Make this panel as tall as possible"
msgstr "Make this panel as tall as possible"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:55
msgid "Make this panel as wide as possible"
msgstr "Make this panel as wide as possible"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:63
msgctxt "@action:button Delete the panel"
msgid "Delete"
msgstr "Delete"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:66
msgid "Remove this panel; this action is undo-able"
msgstr "Remove this panel; this action is undo-able"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:92
msgid "Alignment:"
msgstr "Alignment:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:98
msgid "Top"
msgstr "Top"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:98
msgid "Left"
msgstr "Left"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:99
msgid ""
"Aligns a non-maximized panel to the top; no effect when panel is maximized"
msgstr ""
"Aligns a non-maximised panel to the top; no effect when panel is maximised"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:99
msgid ""
"Aligns a non-maximized panel to the left; no effect when panel is maximized"
msgstr ""
"Aligns a non-maximised panel to the left; no effect when panel is maximised"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:107
msgid "Center"
msgstr "Centre"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:108
msgid ""
"Aligns a non-maximized panel to the center; no effect when panel is maximized"
msgstr ""
"Aligns a non-maximised panel to the centre; no effect when panel is maximised"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:116
msgid "Bottom"
msgstr "Bottom"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:116
msgid "Right"
msgstr "Right"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:117
msgid ""
"Aligns a non-maximized panel to the bottom; no effect when panel is maximized"
msgstr ""
"Aligns a non-maximised panel to the bottom; no effect when panel is maximised"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:117
msgid ""
"Aligns a non-maximized panel to the right; no effect when panel is maximized"
msgstr ""
"Aligns a non-maximised panel to the right; no effect when panel is maximised"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:150
msgid "Visibility:"
msgstr "Visibility:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:155
msgid "Always Visible"
msgstr "Always Visible"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:163
msgid "Auto-Hide"
msgstr "Auto-Hide"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:164
msgid ""
"Panel is hidden, but reveals itself when the cursor touches the panel's "
"screen edge"
msgstr ""
"Panel is hidden, but reveals itself when the cursor touches the panel's "
"screen edge"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:193
msgid "Opacity:"
msgstr "Opacity:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:198
msgid "Always Opaque"
msgstr "Always Opaque"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:206
msgid "Adaptive"
msgstr "Adaptive"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:207
msgid ""
"Panel is opaque when any windows are touching it, and translucent at other "
"times"
msgstr ""
"Panel is opaque when any windows are touching it, and translucent at other "
"times"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:216
msgid "Always Translucent"
msgstr "Always Translucent"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:245
msgid "Floating:"
msgstr "Floating:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:250
msgid "Floating"
msgstr "Floating"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:251
msgid "Panel visibly floats away from its screen edge"
msgstr "Panel visibly floats away from its screen edge"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:260
msgid "Attached"
msgstr "Attached"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:261
msgid "Panel is attached to its screen edge"
msgstr "Panel is attached to its screen edge"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:283
msgid "Focus Shortcut:"
msgstr "Focus Shortcut:"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:293
msgid "Press this keyboard shortcut to move focus to the Panel"
msgstr "Press this keyboard shortcut to move focus to the Panel"

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum height."
msgstr "Drag to change maximum height."

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum width."
msgstr "Drag to change maximum width."

#: contents/configuration/panelconfiguration/Ruler.qml:20
#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Double click to reset."
msgstr "Double click to reset."

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum height."
msgstr "Drag to change minimum height."

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum width."
msgstr "Drag to change minimum width."

#: contents/configuration/panelconfiguration/Ruler.qml:69
msgid ""
"Drag to change position on this screen edge.\n"
"Double click to reset."
msgstr ""
"Drag to change position on this screen edge.\n"
"Double click to reset."

#: contents/configuration/panelconfiguration/ToolBar.qml:24
msgid "Add Widgets…"
msgstr "Add Widgets…"

#: contents/configuration/panelconfiguration/ToolBar.qml:25
msgid "Add Spacer"
msgstr "Add Spacer"

#: contents/configuration/panelconfiguration/ToolBar.qml:26
msgid "More Options…"
msgstr "More Options…"

#: contents/configuration/panelconfiguration/ToolBar.qml:222
msgctxt "Minimize the length of this string as much as possible"
msgid "Drag to move"
msgstr "Drag to move"

#: contents/configuration/panelconfiguration/ToolBar.qml:261
msgctxt "@info:tooltip"
msgid "Use arrow keys to move the panel"
msgstr "Use arrow keys to move the panel"

#: contents/configuration/panelconfiguration/ToolBar.qml:282
msgid "Panel width:"
msgstr "Panel width:"

#: contents/configuration/panelconfiguration/ToolBar.qml:282
msgid "Panel height:"
msgstr "Panel height:"

#: contents/configuration/panelconfiguration/ToolBar.qml:402
#: contents/configuration/ShellContainmentConfiguration.qml:43
msgid "Close"
msgstr "Close"

#: contents/configuration/ShellContainmentConfiguration.qml:19
msgid "Panels and Desktops Management"
msgstr "Panels and Desktops Management"

#: contents/configuration/ShellContainmentConfiguration.qml:34
msgid ""
"You can drag Panels and Desktops around to move them to different screens."
msgstr ""
"You can drag Panels and Desktops around to move them to different screens."

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:178
msgid "Swap with Desktop on Screen %1"
msgstr "Swap with Desktop on Screen %1"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:179
msgid "Move to Screen %1"
msgstr "Move to Screen %1"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:193
msgid "Remove Desktop"
msgstr "Remove Desktop"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:194
msgid "Remove Panel"
msgstr "Remove Panel"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:267
msgid "%1 (primary)"
msgstr "%1 (primary)"

#: contents/explorer/AppletAlternatives.qml:63
msgid "Alternative Widgets"
msgstr "Alternative Widgets"

#: contents/explorer/AppletAlternatives.qml:163
msgid "Switch"
msgstr "Switch"

#: contents/explorer/AppletDelegate.qml:177
msgid "Undo uninstall"
msgstr "Undo uninstall"

#: contents/explorer/AppletDelegate.qml:178
msgid "Uninstall widget"
msgstr "Uninstall widget"

#: contents/explorer/Tooltip.qml:102
msgid "Author:"
msgstr "Author:"

#: contents/explorer/Tooltip.qml:110
msgid "Email:"
msgstr "Email:"

#: contents/explorer/Tooltip.qml:129
msgid "Uninstall"
msgstr "Uninstall"

#: contents/explorer/WidgetExplorer.qml:129
#: contents/explorer/WidgetExplorer.qml:240
msgid "All Widgets"
msgstr "All Widgets"

#: contents/explorer/WidgetExplorer.qml:194
msgid "Widgets"
msgstr "Widgets"

#: contents/explorer/WidgetExplorer.qml:202
msgid "Get New Widgets…"
msgstr "Get New Widgets…"

#: contents/explorer/WidgetExplorer.qml:251
msgid "Categories"
msgstr "Categories"

#: contents/explorer/WidgetExplorer.qml:331
msgid "No widgets matched the search terms"
msgstr "No widgets matched the search terms"

#: contents/explorer/WidgetExplorer.qml:331
msgid "No widgets available"
msgstr "No widgets available"

#, fuzzy
#~| msgid "Windows Behind"
#~ msgid "Windows Above"
#~ msgstr "Windows Behind"

#, fuzzy
#~| msgid "Windows Behind"
#~ msgid "Windows Below"
#~ msgstr "Windows Behind"

#, fuzzy
#~| msgid ""
#~| "Makes the panel remain visible always but part of the maximized windows "
#~| "shall go below the panel as though the panel did not exist."
#~ msgid ""
#~ "Like \"Always Visible\", but maximized and tiled windows go under the "
#~ "panel as though it didn't exist"
#~ msgstr ""
#~ "Makes the panel remain visible always but part of the maximised windows "
#~ "shall go below the panel as though the panel did not exist."

#~ msgid "Windows In Front"
#~ msgstr "Windows In Front"

#~ msgid "Aligns the panel"
#~ msgstr "Aligns the panel"

#~ msgid "Center aligns the panel if the panel is not maximized."
#~ msgstr "Centre aligns the panel if the panel is not maximised."

#~ msgid ""
#~ "Makes the panel hidden always but reveals it when mouse enters the area "
#~ "where the panel would have been if it were not hidden."
#~ msgstr ""
#~ "Makes the panel hidden always but reveals it when mouse enters the area "
#~ "where the panel would have been if it were not hidden."

#~ msgid ""
#~ "Makes the panel remain visible always but maximized windows shall cover "
#~ "it. It is revealed when mouse enters the area where the panel would have "
#~ "been if it were not covered."
#~ msgstr ""
#~ "Makes the panel remain visible always but maximised windows shall cover "
#~ "it. It is revealed when mouse enters the area where the panel would have "
#~ "been if it were not covered."

#~ msgid "Makes the panel translucent except when some windows touch it."
#~ msgstr "Makes the panel translucent except when some windows touch it."

#~ msgid "Makes the panel translucent always."
#~ msgstr "Makes the panel translucent always."

#~ msgid "Makes the panel float from the edge of the screen."
#~ msgstr "Makes the panel float from the edge of the screen."

#~ msgid "Makes the panel remain attached to the edge of the screen."
#~ msgstr "Makes the panel remain attached to the edge of the screen."

#~ msgid "%1 (disabled)"
#~ msgstr "%1 (disabled)"

#~ msgid "Appearance"
#~ msgstr "Appearance"

#~ msgid "Search…"
#~ msgstr "Search…"

#~ msgid "Screen Edge"
#~ msgstr "Screen Edge"

#~ msgid "Click and drag the button to a screen edge to move the panel there."
#~ msgstr "Click and drag the button to a screen edge to move the panel there."

#~ msgid "Width"
#~ msgstr "Width"

#~ msgid "Height"
#~ msgstr "Height"

#~ msgid "Click and drag the button to resize the panel."
#~ msgstr "Click and drag the button to resize the panel."

#~ msgid "Layout cannot be changed while widgets are locked"
#~ msgstr "Layout cannot be changed while widgets are locked"

#~ msgid "Lock Widgets"
#~ msgstr "Lock Widgets"

#~ msgid ""
#~ "This shortcut will activate the applet: it will give the keyboard focus "
#~ "to it, and if the applet has a popup (such as the start menu), the popup "
#~ "will be open."
#~ msgstr ""
#~ "This shortcut will activate the applet: it will give the keyboard focus "
#~ "to it, and if the applet has a popup (such as the start menu), the popup "
#~ "will be open."

#~ msgid "Stop"
#~ msgstr "Stop"

#~ msgid "Activity name:"
#~ msgstr "Activity name:"

#~ msgid "Create"
#~ msgstr "Create"

#~ msgid "Are you sure you want to delete this activity?"
#~ msgstr "Are you sure you want to delete this activity?"
