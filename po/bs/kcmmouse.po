# translation of kcminput.po to bosanski
# Bosnian translation for kdebase-workspace
# Copyright (c) 2010 Rosetta Contributors and Canonical Ltd 2010
# This file is distributed under the same license as the kdebase-workspace package.
#
# FIRST AUTHOR <EMAIL@ADDRESS>, 2010.
# KDE 4 <megaribi@epn.ba>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: kcminput\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-17 02:33+0000\n"
"PO-Revision-Date: 2015-01-07 00:17+0000\n"
"Last-Translator: Samir Ribić <Unknown>\n"
"Language-Team: bosanski <bs@li.org>\n"
"Language: bs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Launchpad (build 17341)\n"
"X-Launchpad-Export-Date: 2015-02-15 05:58+0000\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Samir Ribić"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "Samir.ribic@etf.unsa.ba"

#: backends/kwin_wl/kwin_wl_backend.cpp:66
#, kde-format
msgid "Querying input devices failed. Please reopen this settings module."
msgstr ""

#: backends/kwin_wl/kwin_wl_backend.cpp:86
#, kde-format
msgid "Critical error on reading fundamental device infos of %1."
msgstr ""

#: kcm/libinput/libinput_config.cpp:90
#, kde-format
msgid ""
"Error while loading values. See logs for more information. Please restart "
"this configuration module."
msgstr ""

#: kcm/libinput/libinput_config.cpp:95
#, kde-format
msgid "No pointer device found. Connect now."
msgstr ""

#: kcm/libinput/libinput_config.cpp:106
#, kde-format
msgid ""
"Not able to save all changes. See logs for more information. Please restart "
"this configuration module and try again."
msgstr ""

#: kcm/libinput/libinput_config.cpp:126
#, kde-format
msgid ""
"Error while loading default values. Failed to set some options to their "
"default values."
msgstr ""

#: kcm/libinput/libinput_config.cpp:148
#, kde-format
msgid ""
"Error while adding newly connected device. Please reconnect it and restart "
"this configuration module."
msgstr ""

#: kcm/libinput/libinput_config.cpp:172
#, kde-format
msgid "Pointer device disconnected. Closed its setting dialog."
msgstr ""

#: kcm/libinput/libinput_config.cpp:174
#, kde-format
msgid "Pointer device disconnected. No other devices found."
msgstr ""

#: kcm/libinput/main.qml:74
#, kde-format
msgid "Device:"
msgstr ""

#: kcm/libinput/main.qml:98 kcm/libinput/main_deviceless.qml:49
#, fuzzy, kde-format
#| msgid "&General"
msgid "General:"
msgstr "&Opšte"

#: kcm/libinput/main.qml:100
#, kde-format
msgid "Device enabled"
msgstr ""

#: kcm/libinput/main.qml:120
#, kde-format
msgid "Accept input through this device."
msgstr ""

#: kcm/libinput/main.qml:125 kcm/libinput/main_deviceless.qml:51
#, fuzzy, kde-format
#| msgid "Le&ft handed"
msgid "Left handed mode"
msgstr "&Ljevak"

#: kcm/libinput/main.qml:145 kcm/libinput/main_deviceless.qml:71
#, kde-format
msgid "Swap left and right buttons."
msgstr ""

#: kcm/libinput/main.qml:150 kcm/libinput/main_deviceless.qml:76
#, kde-format
msgid "Press left and right buttons for middle-click"
msgstr ""

#: kcm/libinput/main.qml:170 kcm/libinput/main_deviceless.qml:96
#, kde-format
msgid ""
"Clicking left and right button simultaneously sends middle button click."
msgstr ""

#: kcm/libinput/main.qml:179 kcm/libinput/main_deviceless.qml:105
#, fuzzy, kde-format
#| msgid "Pointer threshold:"
msgid "Pointer speed:"
msgstr "Prag povlačenja:"

#: kcm/libinput/main.qml:211 kcm/libinput/main_deviceless.qml:137
#, fuzzy, kde-format
#| msgid "Acceleration &profile:"
msgid "Acceleration profile:"
msgstr "&Profil ubrzanja:"

#: kcm/libinput/main.qml:242 kcm/libinput/main_deviceless.qml:168
#, kde-format
msgid "Flat"
msgstr ""

#: kcm/libinput/main.qml:246 kcm/libinput/main_deviceless.qml:172
#, kde-format
msgid "Cursor moves the same distance as the mouse movement."
msgstr ""

#: kcm/libinput/main.qml:252 kcm/libinput/main_deviceless.qml:178
#, kde-format
msgid "Adaptive"
msgstr ""

#: kcm/libinput/main.qml:256 kcm/libinput/main_deviceless.qml:182
#, kde-format
msgid "Cursor travel distance depends on the mouse movement speed."
msgstr ""

#: kcm/libinput/main.qml:267 kcm/libinput/main_deviceless.qml:193
#, kde-format
msgid "Scrolling:"
msgstr ""

#: kcm/libinput/main.qml:269 kcm/libinput/main_deviceless.qml:195
#, fuzzy, kde-format
#| msgid "Re&verse scroll direction"
msgid "Invert scroll direction"
msgstr "O&brnut smjer skrolovanja"

#: kcm/libinput/main.qml:285 kcm/libinput/main_deviceless.qml:211
#, kde-format
msgid "Touchscreen like scrolling."
msgstr ""

#: kcm/libinput/main.qml:290
#, fuzzy, kde-format
#| msgid "Pointer threshold:"
msgid "Scrolling speed:"
msgstr "Prag povlačenja:"

#: kcm/libinput/main.qml:338
#, kde-format
msgctxt "Slower Scroll"
msgid "Slower"
msgstr ""

#: kcm/libinput/main.qml:344
#, kde-format
msgctxt "Faster Scroll Speed"
msgid "Faster"
msgstr ""

#: kcm/libinput/main.qml:355
#, kde-format
msgctxt "@action:button"
msgid "Re-bind Additional Mouse Buttons…"
msgstr ""

#: kcm/libinput/main.qml:391
#, kde-format
msgctxt "@label for assigning an action to a numbered button"
msgid "Extra Button %1:"
msgstr ""

#: kcm/libinput/main.qml:421
#, kde-format
msgctxt "@action:button"
msgid "Press the mouse button for which you want to add a key binding"
msgstr ""

#: kcm/libinput/main.qml:422
#, kde-format
msgctxt "@action:button, %1 is the translation of 'Extra Button %1' from above"
msgid "Enter the new key combination for %1"
msgstr ""

#: kcm/libinput/main.qml:426
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr ""

#: kcm/libinput/main.qml:443
#, fuzzy, kde-format
#| msgid "Press Connect Button"
msgctxt "@action:button"
msgid "Press a mouse button "
msgstr "Pritisnuti dugme Povezati"

#: kcm/libinput/main.qml:444
#, kde-format
msgctxt "@action:button, Bind a mousebutton to keyboard key(s)"
msgid "Add Binding…"
msgstr ""

#: kcm/libinput/main.qml:473
#, kde-format
msgctxt "@action:button"
msgid "Go back"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QWidget, KCMMouse)
#: kcm/xlib/kcmmouse.ui:14
#, kde-format
msgid ""
"<h1>Mouse</h1> This module allows you to choose various options for the way "
"in which your pointing device works. Your pointing device may be a mouse, "
"trackball, or some other hardware that performs a similar function."
msgstr ""
"<h1>Miš</h1> Ovaj modul omogućava razna podešavanja ponašanja uređaja kojim "
"pokrećete kursor (obično miš ili kugla pokreću strelicu)."

#. i18n: ectx: attribute (title), widget (QWidget, generalTab)
#: kcm/xlib/kcmmouse.ui:36
#, kde-format
msgid "&General"
msgstr "&Opšte"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, handedBox)
#: kcm/xlib/kcmmouse.ui:52
#, kde-format
msgid ""
"If you are left-handed, you may prefer to swap the functions of the left and "
"right buttons on your pointing device by choosing the 'left-handed' option. "
"If your pointing device has more than two buttons, only those that function "
"as the left and right buttons are affected. For example, if you have a three-"
"button mouse, the middle button is unaffected."
msgstr ""
"Ako ste ljevak, moglo bi vam odgovarati zamjena značenja lijeve i desne "
"tipke miša tako što odaberete 'za ljevake'. Ako vaš miš ima više od dvije "
"tipke, tada će se zamijeniti samo lijeva i desna tipka, npr. srednja tipka "
"će imati isto djelovanje kao i dotada."

#. i18n: ectx: property (title), widget (QGroupBox, handedBox)
#: kcm/xlib/kcmmouse.ui:55
#, kde-format
msgid "Button Order"
msgstr "Redoslijed dugmadi"

#. i18n: ectx: property (text), widget (QRadioButton, rightHanded)
#: kcm/xlib/kcmmouse.ui:64
#, kde-format
msgid "Righ&t handed"
msgstr "&Dešnjak"

#. i18n: ectx: property (text), widget (QRadioButton, leftHanded)
#: kcm/xlib/kcmmouse.ui:77
#, kde-format
msgid "Le&ft handed"
msgstr "&Ljevak"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, cbScrollPolarity)
#: kcm/xlib/kcmmouse.ui:106
#, kde-format
msgid ""
"Change the direction of scrolling for the mouse wheel or the 4th and 5th "
"mouse buttons."
msgstr ""
"Promijenite smjer skrolovanja miševa sa kuglicom ili četvrtog i petog "
"dugmeta miša."

#. i18n: ectx: property (text), widget (QCheckBox, cbScrollPolarity)
#: kcm/xlib/kcmmouse.ui:109
#, kde-format
msgid "Re&verse scroll direction"
msgstr "O&brnut smjer skrolovanja"

#. i18n: ectx: attribute (title), widget (QWidget, advancedTab)
#: kcm/xlib/kcmmouse.ui:156
#, kde-format
msgid "Advanced"
msgstr "Napredno"

#. i18n: ectx: property (text), widget (QLabel, label)
#: kcm/xlib/kcmmouse.ui:164
#, kde-format
msgid "Pointer acceleration:"
msgstr "Ubrzanje kursora:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: kcm/xlib/kcmmouse.ui:174
#, kde-format
msgid "Pointer threshold:"
msgstr "Prag povlačenja:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: kcm/xlib/kcmmouse.ui:184
#, kde-format
msgid "Double click interval:"
msgstr "Interval za dvostruki klik:"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: kcm/xlib/kcmmouse.ui:194
#, kde-format
msgid "Drag start time:"
msgstr "Vrijeme do početka povlačenja:"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: kcm/xlib/kcmmouse.ui:204
#, kde-format
msgid "Drag start distance:"
msgstr "Početna udaljenost povlačenja:"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: kcm/xlib/kcmmouse.ui:214
#, kde-format
msgid "Mouse wheel scrolls by:"
msgstr "Miš sa kuglicom se pomjera za:"

#. i18n: ectx: property (whatsThis), widget (QDoubleSpinBox, accel)
#: kcm/xlib/kcmmouse.ui:230
#, kde-format
msgid ""
"<p>This option allows you to change the relationship between the distance "
"that the mouse pointer moves on the screen and the relative movement of the "
"physical device itself (which may be a mouse, trackball, or some other "
"pointing device.)</p><p> A high value for the acceleration will lead to "
"large movements of the mouse pointer on the screen even when you only make a "
"small movement with the physical device. Selecting very high values may "
"result in the mouse pointer flying across the screen, making it hard to "
"control.</p>"
msgstr ""
"<qt><p>Ovom opcijom podešavate odnos između rastojanja koji pokazivač "
"prelazi na ekranu i relativnog pomjeraja samog fizičkog uređaja (miša, "
"trekbola, ili nekog drugog pokazivačkog uređaja).</p><p>Visoka vrijednost "
"ubrzanja će dovesti do velikih pomijeranja pokazivača na ekranu čak i kada "
"pravite male pokrete uređajem, čineći ga teškim za kontrolu.</p></qt>"

#. i18n: ectx: property (suffix), widget (QDoubleSpinBox, accel)
#: kcm/xlib/kcmmouse.ui:233
#, kde-format
msgid " x"
msgstr " x"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, thresh)
#: kcm/xlib/kcmmouse.ui:261
#, kde-format
msgid ""
"<p>The threshold is the smallest distance that the mouse pointer must move "
"on the screen before acceleration has any effect. If the movement is smaller "
"than the threshold, the mouse pointer moves as if the acceleration was set "
"to 1X;</p><p> thus, when you make small movements with the physical device, "
"there is no acceleration at all, giving you a greater degree of control over "
"the mouse pointer. With larger movements of the physical device, you can "
"move the mouse pointer rapidly to different areas on the screen.</p>"
msgstr ""
"<qt><p>Prag je najmanja razdaljina koju pokazivač mora da prijeđe na ekranu "
"da bi ubrzanje stupilo u dejstvo. Ako je pomak manji od praga, pokazivač se "
"pomijera kao da je ubrzanje postavljeno na 1×.</p><p>Ako pravite male "
"pokrete fizičkim uređajem, ubrzanja uopšte nema, što vam daje veći stepen "
"kontrole nad pokazivačem. Većim pokretima uređaja možete brzo da pomijerate "
"pokazivač prema udaljenim dijelovima ekrana.</p></qt>"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, doubleClickInterval)
#: kcm/xlib/kcmmouse.ui:280
#, kde-format
msgid ""
"The double click interval is the maximal time (in milliseconds) between two "
"mouse clicks which turns them into a double click. If the second click "
"happens later than this time interval after the first click, they are "
"recognized as two separate clicks."
msgstr ""
"Interval za dvostruki klik je najduže vrijeme koje može proteći između dva "
"klika a da se oni protumače kao dvostruki klik. Ako kliknete poslije "
"navedenog vremena onda se klikovi prepoznaju kao dva zasebna."

#. i18n: ectx: property (suffix), widget (QSpinBox, doubleClickInterval)
#. i18n: ectx: property (suffix), widget (QSpinBox, dragStartTime)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_delay)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_interval)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_time_to_max)
#: kcm/xlib/kcmmouse.ui:283 kcm/xlib/kcmmouse.ui:311 kcm/xlib/kcmmouse.ui:408
#: kcm/xlib/kcmmouse.ui:450 kcm/xlib/kcmmouse.ui:482
#, kde-format
msgid " msec"
msgstr " ms"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, dragStartTime)
#: kcm/xlib/kcmmouse.ui:308
#, kde-format
msgid ""
"If you click with the mouse (e.g. in a multi-line editor) and begin to move "
"the mouse within the drag start time, a drag operation will be initiated."
msgstr ""
"Ako kliknete mišem i počnete ga micati prije navedenog vremena, onda će se "
"početi sa povlačenjem (npr. sa premještanjem odabranog teksta u editoru)."

#. i18n: ectx: property (whatsThis), widget (QSpinBox, dragStartDist)
#: kcm/xlib/kcmmouse.ui:333
#, kde-format
msgid ""
"If you click with the mouse and begin to move the mouse at least the drag "
"start distance, a drag operation will be initiated."
msgstr ""
"Ako kliknete i pomaknete miš barem za navedeni broj tačkica, onda će se "
"početi sa povlačenjem."

#. i18n: ectx: property (whatsThis), widget (QSpinBox, wheelScrollLines)
#: kcm/xlib/kcmmouse.ui:355
#, kde-format
msgid ""
"If you use the wheel of a mouse, this value determines the number of lines "
"to scroll for each wheel movement. Note that if this number exceeds the "
"number of visible lines, it will be ignored and the wheel movement will be "
"handled as a page up/down movement."
msgstr ""
"Ako koristite miš sa kuglicom (wheel) , onda ova vrijednost određuje za "
"koliko redova će kursor biti pomaknut pri svakom pomaku točkića. U slučaju "
"da je taj broj veći od broja redova koji su na raspolaganju, pokret će biti "
"zanemaren."

#. i18n: ectx: attribute (title), widget (QWidget, MouseNavigation)
#: kcm/xlib/kcmmouse.ui:387
#, fuzzy, kde-format
#| msgid "Mouse Navigation"
msgid "Keyboard Navigation"
msgstr "Navigacija miša"

#. i18n: ectx: property (text), widget (QCheckBox, mouseKeys)
#: kcm/xlib/kcmmouse.ui:395
#, kde-format
msgid "&Move pointer with keyboard (using the num pad)"
msgstr "&Pomjeranje miša tastaturom (koristeći numeričke tipke)"

#. i18n: ectx: property (text), widget (QLabel, label_7)
#: kcm/xlib/kcmmouse.ui:424
#, kde-format
msgid "&Acceleration delay:"
msgstr "Kašnjenje ubrzanj&a:"

#. i18n: ectx: property (text), widget (QLabel, label_8)
#: kcm/xlib/kcmmouse.ui:434
#, kde-format
msgid "R&epeat interval:"
msgstr "Int&erval ponavljanja:"

#. i18n: ectx: property (text), widget (QLabel, label_9)
#: kcm/xlib/kcmmouse.ui:466
#, kde-format
msgid "Acceleration &time:"
msgstr "&Vrijeme ubrzanja:"

#. i18n: ectx: property (text), widget (QLabel, label_10)
#: kcm/xlib/kcmmouse.ui:498
#, kde-format
msgid "Ma&ximum speed:"
msgstr "Ma&ksimalna brzina:"

#. i18n: ectx: property (suffix), widget (QSpinBox, mk_max_speed)
#: kcm/xlib/kcmmouse.ui:514
#, kde-format
msgid " pixel/sec"
msgstr " piksela/sek"

#. i18n: ectx: property (text), widget (QLabel, label_11)
#: kcm/xlib/kcmmouse.ui:530
#, kde-format
msgid "Acceleration &profile:"
msgstr "&Profil ubrzanja:"

#: kcm/xlib/xlib_config.cpp:267 kcm/xlib/xlib_config.cpp:272
#, kde-format
msgid " pixel"
msgid_plural " pixels"
msgstr[0] " piksel"
msgstr[1] " piksela"
msgstr[2] " piksela"

#: kcm/xlib/xlib_config.cpp:277
#, kde-format
msgid " line"
msgid_plural " lines"
msgstr[0] " linija"
msgstr[1] " linije"
msgstr[2] " linija"

#~ msgid "Mouse"
#~ msgstr "Miš"

#, fuzzy
#~| msgid "(c) 1997 - 2005 Mouse developers"
#~ msgid "(c) 1997 - 2018 Mouse developers"
#~ msgstr "© 1997-2005, razvijači Mouse-a"

#~ msgid "Patrick Dowler"
#~ msgstr "Patrick Dowler"

#~ msgid "Dirk A. Mueller"
#~ msgstr "Dirk A. Mueller"

#~ msgid "David Faure"
#~ msgstr "David Faure"

#~ msgid "Bernd Gehrmann"
#~ msgstr "Bernd Gehrmann"

#~ msgid "Rik Hemsley"
#~ msgstr "Rik Hemsley"

#~ msgid "Brad Hughes"
#~ msgstr "Brad Hughes"

#~ msgid "Brad Hards"
#~ msgstr "Brad Hards"

#~ msgid "Ralf Nolden"
#~ msgstr "Ralf Nolden"

#, fuzzy
#~| msgid "Acceleration &time:"
#~ msgid "Acceleration:"
#~ msgstr "&Vrijeme ubrzanja:"

#, fuzzy
#~| msgid "Acceleration &profile:"
#~ msgid "Acceleration Profile:"
#~ msgstr "&Profil ubrzanja:"

#~ msgid "Icons"
#~ msgstr "Ikone"

#~ msgid ""
#~ "The default behavior in KDE is to select and activate icons with a single "
#~ "click of the left button on your pointing device. This behavior is "
#~ "consistent with what you would expect when you click links in most web "
#~ "browsers. If you would prefer to select with a single click, and activate "
#~ "with a double click, check this option."
#~ msgstr ""
#~ "Obično se u KDE-u ikone odabiru i aktiviraju jednim klikom lijeve tipke "
#~ "vašeg miša, što je slično onome u pretraživačima za internet. Ipak, ako "
#~ "želite odabiranje sa jednim, a aktiviranje sa dva klika, onda odaberite "
#~ "ovu mogućnost ovdje."

#~ msgid ""
#~ "Dou&ble-click to open files and folders (select icons on first click)"
#~ msgstr ""
#~ "Kliknite d&va puta da bi se otvorile datoteke i direktoriji (jednim "
#~ "klikom  odabrati)"

#~ msgid "Activates and opens a file or folder with a single click."
#~ msgstr "Aktivira ili otvara datoteku ili direktorij sa jednim klikom."

#~ msgid "&Single-click to open files and folders"
#~ msgstr "&Jednostruki klik otvara datoteke i direktorije"

#~ msgid "Select the cursor theme you want to use:"
#~ msgstr "Izaberite temu kursora koju želite koristiti:"

#~ msgid "Name"
#~ msgstr "Ime"

#~ msgid "Description"
#~ msgstr "Opis"

#~ msgid "You have to restart KDE for these changes to take effect."
#~ msgstr "Morate restartovati KDE kako bi ove izmjene imale efekta."

#~ msgid "Cursor Settings Changed"
#~ msgstr "Postavke kursora promijenjene"

#~ msgid "Small black"
#~ msgstr "Mali crni"

#~ msgid "Small black cursors"
#~ msgstr "Mali crni kursori"

#~ msgid "Large black"
#~ msgstr "Veliki crni"

#~ msgid "Large black cursors"
#~ msgstr "Veliki crni kursori"

#~ msgid "Small white"
#~ msgstr "Mali bijeli"

#~ msgid "Small white cursors"
#~ msgstr "Mali bijeli kursori"

#~ msgid "Large white"
#~ msgstr "Veliki bijeli"

#~ msgid "Large white cursors"
#~ msgstr "Veliki bijeli kursori"

#~ msgid "Cha&nge pointer shape over icons"
#~ msgstr "Promijeni o&blik kursora kada se kursor nalazi iznad ikona"

#~ msgid "A&utomatically select icons"
#~ msgstr "Auto&matski odabrati ikone"

#~ msgctxt "label. delay (on milliseconds) to automatically select icons"
#~ msgid "Delay"
#~ msgstr "Kašnjenje"

#~ msgctxt "milliseconds. time to automatically select the items"
#~ msgid " ms"
#~ msgstr " ms"

#~ msgid ""
#~ "If you check this option, pausing the mouse pointer over an icon on the "
#~ "screen will automatically select that icon. This may be useful when "
#~ "single clicks activate icons, and you want only to select the icon "
#~ "without activating it."
#~ msgstr ""
#~ "Ako odaberete ovu mogućnost, onda će duže zadržavanje miša iznad ikone "
#~ "automatski odabrati ikonu. To je zgodno kada koristite jedan klik za "
#~ "pokretanje, a ne želite ikonu pokrenuti, nego samo odabrati."

#~ msgid ""
#~ "If you have checked the option to automatically select icons, this slider "
#~ "allows you to select how long the mouse pointer must be paused over the "
#~ "icon before it is selected."
#~ msgstr ""
#~ "Ako odaberete ovu mogućnost automatskog odabiranja ikona, onda vam ovaj "
#~ "klizač (slider) podešava koliko dugo mora miš biti iznad ikone da bi ona "
#~ "bila odabrana."

#~ msgid "Mouse type: %1"
#~ msgstr "Vrsta miša: %1"

#~ msgid ""
#~ "RF channel 1 has been set. Please press Connect button on mouse to re-"
#~ "establish link"
#~ msgstr ""
#~ "RF kanal 1 je podešen. Pritisnuti dugme Povezati na mišu kako bi se "
#~ "uspostavila veza"

#~ msgid ""
#~ "RF channel 2 has been set. Please press Connect button on mouse to re-"
#~ "establish link"
#~ msgstr ""
#~ "RF kanal 2 je podešen. Pritisnuti dugme Povezati na mišu kako bi se "
#~ "uspostavila veza"

#~ msgctxt "no cordless mouse"
#~ msgid "none"
#~ msgstr "ništa"

#~ msgid "Cordless Mouse"
#~ msgstr "Bežični miš"

#~ msgid "Cordless Wheel Mouse"
#~ msgstr "Bežični miš sa kuglicom"

#~ msgid "Cordless MouseMan Wheel"
#~ msgstr "Bežični MouseMan sa kuglicom"

#~ msgid "Cordless TrackMan Wheel"
#~ msgstr "Bežični TrackMan sa kuglicom"

#~ msgid "TrackMan Live"
#~ msgstr "TrackMan Live"

#~ msgid "Cordless TrackMan FX"
#~ msgstr "Bežični TrackMan FX"

#~ msgid "Cordless MouseMan Optical"
#~ msgstr "Bežični, optički MouseMan"

#~ msgid "Cordless Optical Mouse"
#~ msgstr "Bežični, optički miš"

#~ msgid "Cordless MouseMan Optical (2ch)"
#~ msgstr "Bežični, optički MouseMan (2ch)"

#~ msgid "Cordless Optical Mouse (2ch)"
#~ msgstr "Bežični, optički miš (2ch)"

#~ msgid "Cordless Mouse (2ch)"
#~ msgstr "Bežični miš (2ch)"

#~ msgid "Cordless Optical TrackMan"
#~ msgstr "Bežični, optički TrackMan"

#~ msgid "MX700 Cordless Optical Mouse"
#~ msgstr "Bežični, optički MX700 miš"

#~ msgid "MX700 Cordless Optical Mouse (2ch)"
#~ msgstr "Bežični, optički MX700 miš (2ch)"

#~ msgid "Unknown mouse"
#~ msgstr "Nepoznat miš"

#~ msgid "Cordless Name"
#~ msgstr "Bežični naziv"

#~ msgid "Sensor Resolution"
#~ msgstr "Rezolucija senzora"

#~ msgid "400 counts per inch"
#~ msgstr "400 tačkica (counts) po inču"

#~ msgid "800 counts per inch"
#~ msgstr "800 tačkica (counts) po inču"

#~ msgid "Battery Level"
#~ msgstr "Nivo baterije"

#~ msgid "RF Channel"
#~ msgstr "RF kanal"

#~ msgid "Channel 1"
#~ msgstr "Kanal 1"

#~ msgid "Channel 2"
#~ msgstr "Kanal 2"

#~ msgid ""
#~ "You have a Logitech Mouse connected, and libusb was found at compile "
#~ "time, but it was not possible to access this mouse. This is probably "
#~ "caused by a permissions problem - you should consult the manual on how to "
#~ "fix this."
#~ msgstr ""
#~ "Imate Logitech-ov miš i libusb je pronađen prilikom kompajliranja, ali "
#~ "nije moguće pristupiti mišu. Najvjerovanije se radi o problemu sa "
#~ "dozvolama - pogledajte u priručniku kako riješiti ovaj problem."

#~ msgid "Cursor Theme"
#~ msgstr "Tema kursora"

#~ msgid "(c) 2003-2007 Fredrik Höglund"
#~ msgstr "(c) 2003-2007 Fredrik Höglund"

#~ msgid "Fredrik Höglund"
#~ msgstr "Fredrik Höglund"

#, fuzzy
#~ msgctxt "@item:inlistbox size"
#~ msgid "Resolution dependent"
#~ msgstr "zavisno od rezolucije"

#~ msgid "Drag or Type Theme URL"
#~ msgstr "Privuci ili otkucaj URL teme"

#~ msgid "Unable to find the cursor theme archive %1."
#~ msgstr "Ne mogu nađi arhivu tema kursora %1!."

#~ msgid ""
#~ "Unable to download the cursor theme archive; please check that the "
#~ "address %1 is correct."
#~ msgstr ""
#~ "Ne mogu da preuzmem arhivu teme pokazivača. Provjerite da li je adresa %1 "
#~ "ispravna."

#~ msgid "The file %1 does not appear to be a valid cursor theme archive."
#~ msgstr "Izgleda da datoteka %1 nije ispravna arhiva teme kursora."

#~ msgid ""
#~ "<qt>You cannot delete the theme you are currently using.<br />You have to "
#~ "switch to another theme first.</qt>"
#~ msgstr ""
#~ "<qt>Ne možete obrisati temu koju trenutno koristite. Prvo morate "
#~ "prebaciti na neku drugu temu.</qt>"

#~ msgid ""
#~ "<qt>Are you sure you want to remove the <i>%1</i> cursor theme?<br />This "
#~ "will delete all the files installed by this theme.</qt>"
#~ msgstr ""
#~ "<qt>Želite li zaista da uklonite temu pokazivača <i>%1</i>? Ovo će "
#~ "obrisati sve datoteke instalirane ovom temom.</qt>"

#~ msgid "Confirmation"
#~ msgstr "Potvrda"

#~ msgid ""
#~ "A theme named %1 already exists in your icon theme folder. Do you want "
#~ "replace it with this one?"
#~ msgstr ""
#~ "Tema pod imenom %1 već postoji u direktoriju tema ikona.  Zamijeniti je "
#~ "ovom?"

#~ msgid "Overwrite Theme?"
#~ msgstr "Prebrisati temu?"

#~ msgid ""
#~ "Select the cursor theme you want to use (hover preview to test cursor):"
#~ msgstr ""
#~ "Izaberite temu kursora koju želite koristiti (pređite preko pregleda da "
#~ "isprobate):"

#~ msgid "Get new color schemes from the Internet"
#~ msgstr "Preuzmi nove šeme boja s Interneta"

#~ msgid "Get New Theme..."
#~ msgstr "Dobavi Novu Temu..."

#~ msgid "Install From File..."
#~ msgstr "Instaliraj iz datoteke..."

#~ msgid "Remove Theme"
#~ msgstr "Ukloni temu"

#~ msgctxt "@label:listbox cursor size"
#~ msgid "Size:"
#~ msgstr "Veličina:"

#~ msgctxt ""
#~ "@info The argument is the list of available sizes (in pixel). Example: "
#~ "'Available sizes: 24' or 'Available sizes: 24, 36, 48'"
#~ msgid "(Available sizes: %1)"
#~ msgstr "(Dostupne veličine: %1)"
