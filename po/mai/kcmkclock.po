# translation of kcmkclock.po to Maithili
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sangeeta Kumari <sangeeta09@gmail.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kcmkclock\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-02-27 00:49+0000\n"
"PO-Revision-Date: 2009-01-18 15:29+0530\n"
"Last-Translator: Sangeeta Kumari <sangeeta09@gmail.com>\n"
"Language-Team: Maithili <maithili.sf.net>\n"
"Language: mai\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"\n"
"\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "संगीता कुमारी"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sangeeta09@gmail.com"

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: dateandtime.ui:22
#, kde-format
msgid "Date and Time"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, setDateTimeAuto)
#: dateandtime.ui:30
#, fuzzy, kde-format
#| msgid "Set date and time &automatically:"
msgid "Set date and time &automatically"
msgstr "तिथि आओर समय केँ स्वतः नियत करू: (&a)"

#. i18n: ectx: property (text), widget (QLabel, timeServerLabel)
#: dateandtime.ui:53
#, kde-format
msgid "&Time server:"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (KDatePicker, cal)
#: dateandtime.ui:86
#, kde-format
msgid "Here you can change the system date's day of the month, month and year."
msgstr "एतए अहाँ तंत्रक तिथि, दिन, माह तथा वर्ष परिवर्तित कए सकैत छी."

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: dateandtime.ui:122
#, kde-format
msgid "Time Zone"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label)
#: dateandtime.ui:128
#, kde-format
msgid "To change the local time zone, select your area from the list below."
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, m_local)
#: dateandtime.ui:151
#, fuzzy, kde-format
#| msgid "Current Maintainer"
msgid "Current local time zone:"
msgstr "वर्तमान मेंटेनर"

#. i18n: ectx: property (placeholderText), widget (KTreeWidgetSearchLine, tzonesearch)
#: dateandtime.ui:161
#, kde-format
msgid "Search…"
msgstr ""

#: dtime.cpp:61
#, kde-format
msgid ""
"No NTP utility has been found. Install 'ntpdate' or 'rdate' command to "
"enable automatic updating of date and time."
msgstr ""

#: dtime.cpp:91
#, kde-format
msgid ""
"Here you can change the system time. Click into the hours, minutes or "
"seconds field to change the relevant value, either using the up and down "
"buttons to the right or by entering a new value."
msgstr ""
"एतए अहाँ तंत्र क' समय परिवर्तित कए सकैत अछि. घंटा, मिनट अथवा सेकण्ड मे क्लिक करू फिनु "
"दहिन्ना देल गेल उप्पर अथवा नीच्चाँ बटन केर उपयोग सँ अथवा नवीन मान भर कए संबंधित समय "
"बदलू."

#: dtime.cpp:113
#, fuzzy, kde-format
#| msgid "Current Maintainer"
msgctxt "%1 is name of time zone"
msgid "Current local time zone: %1"
msgstr "वर्तमान मेंटेनर"

#: dtime.cpp:116
#, fuzzy, kde-format
#| msgid "Current Maintainer"
msgctxt "%1 is name of time zone, %2 is its abbreviation"
msgid "Current local time zone: %1 (%2)"
msgstr "वर्तमान मेंटेनर"

#: dtime.cpp:203
#, fuzzy, kde-format
#| msgid ""
#| "Public Time Server (pool.ntp.org),asia.pool.ntp.org,europe.pool.ntp.org,"
#| "north-america.pool.ntp.org,oceania.pool.ntp.org"
msgid ""
"Public Time Server (pool.ntp.org),        asia.pool.ntp.org,        europe."
"pool.ntp.org,        north-america.pool.ntp.org,        oceania.pool.ntp.org"
msgstr ""
"पब्लिक टाइम सर्वर (pool.ntp.org),asia.pool.ntp.org,europe.pool.ntp.org,north-"
"america.pool.ntp.org,oceania.pool.ntp.org"

#: dtime.cpp:274
#, kde-format
msgid "Unable to contact time server: %1."
msgstr "समय सर्वर : %1 सँ जुडब मे असमर्थ."

#: dtime.cpp:278
#, kde-format
msgid "Can not set date."
msgstr "तिथि नियत नहि कए सकैत अछि."

#: dtime.cpp:281
#, kde-format
msgid "Error setting new time zone."
msgstr ""

#: dtime.cpp:281
#, kde-format
msgid "Time zone Error"
msgstr ""

#: dtime.cpp:299
#, kde-format
msgid ""
"<h1>Date & Time</h1> This system settings module can be used to set the "
"system date and time. As these settings do not only affect you as a user, "
"but rather the whole system, you can only change these settings when you "
"start the System Settings as root. If you do not have the root password, but "
"feel the system time should be corrected, please contact your system "
"administrator."
msgstr ""

#: main.cpp:49
#, kde-format
msgid "KDE Clock Control Module"
msgstr "केडीई घडी नियंत्रण मोड्यूल"

#: main.cpp:53
#, kde-format
msgid "(c) 1996 - 2001 Luca Montecchiani"
msgstr "(c) 1996 - 2001 लुका मान्तेचियानी"

#: main.cpp:55
#, kde-format
msgid "Luca Montecchiani"
msgstr "लुका मान्तेचियानी"

#: main.cpp:55
#, kde-format
msgid "Original author"
msgstr "मूल लेखक"

#: main.cpp:56
#, kde-format
msgid "Paul Campbell"
msgstr "पाल कैम्पबेल"

#: main.cpp:56
#, kde-format
msgid "Current Maintainer"
msgstr "वर्तमान मेंटेनर"

#: main.cpp:57
#, kde-format
msgid "Benjamin Meyer"
msgstr "बेंजामिन मेयर"

#: main.cpp:57
#, kde-format
msgid "Added NTP support"
msgstr "एनटीपी समर्थन जोड़ल गेल अछि"

#: main.cpp:60
#, kde-format
msgid ""
"<h1>Date & Time</h1> This control module can be used to set the system date "
"and time. As these settings do not only affect you as a user, but rather the "
"whole system, you can only change these settings when you start the System "
"Settings as root. If you do not have the root password, but feel the system "
"time should be corrected, please contact your system administrator."
msgstr ""

#: main.cpp:113
#, kde-format
msgid "Unable to authenticate/execute the action: %1, %2"
msgstr ""

#: main.cpp:133
#, kde-format
msgid "Unable to change NTP settings"
msgstr ""

#: main.cpp:144
#, fuzzy, kde-format
#| msgid "Unable to contact time server: %1."
msgid "Unable to set current time"
msgstr "समय सर्वर : %1 सँ जुडब मे असमर्थ."

#: main.cpp:154
#, fuzzy, kde-format
#| msgid "Unable to contact time server: %1."
msgid "Unable to set timezone"
msgstr "समय सर्वर : %1 सँ जुडब मे असमर्थ."

#~ msgid "kcmclock"
#~ msgstr "केसीएमक्लाक"
