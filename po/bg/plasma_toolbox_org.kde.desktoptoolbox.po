# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Mincho Kondarev <mkondarev@yahoo.de>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-08 02:24+0000\n"
"PO-Revision-Date: 2023-01-07 17:58+0100\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.12.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: contents/ui/ToolBoxContent.qml:265
#, kde-format
msgid "Choose Global Theme…"
msgstr "Избиране на глобална тема…"

#: contents/ui/ToolBoxContent.qml:272
#, kde-format
msgid "Configure Display Settings…"
msgstr "Настройки на екрана…"

#: contents/ui/ToolBoxContent.qml:293
#, kde-format
msgctxt "@action:button"
msgid "More"
msgstr "Още"

#: contents/ui/ToolBoxContent.qml:308
#, kde-format
msgid "Exit Edit Mode"
msgstr "Излизане от режима за редактиране"
