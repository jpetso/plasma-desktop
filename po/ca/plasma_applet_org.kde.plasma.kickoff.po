# Translation of plasma_applet_org.kde.plasma.kickoff.po to Catalan
# Copyright (C) 2014-2023 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2014, 2015, 2020, 2022.
# Josep M. Ferrer <txemaq@gmail.com>, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023.
# Empar Montoro Martín <montoro_mde@gva.es>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-12 02:13+0000\n"
"PO-Revision-Date: 2023-01-10 10:43+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 20.12.0\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "General"

#: package/contents/ui/code/tools.js:49
#, kde-format
msgid "Remove from Favorites"
msgstr "Elimina de les preferides"

#: package/contents/ui/code/tools.js:53
#, kde-format
msgid "Add to Favorites"
msgstr "Afegeix a les preferides"

#: package/contents/ui/code/tools.js:77
#, kde-format
msgid "On All Activities"
msgstr "A totes les activitats"

#: package/contents/ui/code/tools.js:127
#, kde-format
msgid "On the Current Activity"
msgstr "A l'activitat actual"

#: package/contents/ui/code/tools.js:141
#, kde-format
msgid "Show in Favorites"
msgstr "Mostra als preferits"

#: package/contents/ui/ConfigGeneral.qml:38
#, kde-format
msgid "Icon:"
msgstr "Icona:"

#: package/contents/ui/ConfigGeneral.qml:44
#, kde-format
msgctxt "@action:button"
msgid "Change Application Launcher's icon"
msgstr "Canvia la icona del llançador d'aplicacions"

#: package/contents/ui/ConfigGeneral.qml:45
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"Current icon is %1. Click to open menu to change the current icon or reset "
"to the default icon."
msgstr ""
"La icona actual és %1. Feu-hi clic per a obrir un menú per canviar la icona "
"actual o restablir la icona predeterminada."

#: package/contents/ui/ConfigGeneral.qml:49
#, kde-format
msgctxt "@info:tooltip"
msgid "Icon name is \"%1\""
msgstr "El nom de la icona és «%1»"

#: package/contents/ui/ConfigGeneral.qml:82
#, kde-format
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr "Trieu…"

#: package/contents/ui/ConfigGeneral.qml:84
#, kde-format
msgctxt "@info:whatsthis"
msgid "Choose an icon for Application Launcher"
msgstr "Trieu una icona per al llançador d'aplicacions"

#: package/contents/ui/ConfigGeneral.qml:88
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Reset to default icon"
msgstr "Restableix a la icona predeterminada"

#: package/contents/ui/ConfigGeneral.qml:94
#, kde-format
msgctxt "@action:inmenu"
msgid "Remove icon"
msgstr "Elimina la icona"

#: package/contents/ui/ConfigGeneral.qml:105
#, kde-format
msgctxt "@label:textbox"
msgid "Text label:"
msgstr "Etiqueta de text:"

#: package/contents/ui/ConfigGeneral.qml:107
#, kde-format
msgctxt "@info:placeholder"
msgid "Type here to add a text label"
msgstr "Teclegeu aquí per afegir una etiqueta de text"

#: package/contents/ui/ConfigGeneral.qml:122
#, kde-format
msgctxt "@action:button"
msgid "Reset menu label"
msgstr "Inicialitza l'etiqueta del menú"

#: package/contents/ui/ConfigGeneral.qml:136
#, kde-format
msgctxt "@info"
msgid "A text label cannot be set when the Panel is vertical."
msgstr "No és pot definir una etiqueta de text quan el plafó és vertical."

#: package/contents/ui/ConfigGeneral.qml:147
#, kde-format
msgctxt "General options"
msgid "General:"
msgstr "General:"

#: package/contents/ui/ConfigGeneral.qml:148
#, kde-format
msgid "Always sort applications alphabetically"
msgstr "Ordena alfabèticament les aplicacions sempre"

#: package/contents/ui/ConfigGeneral.qml:153
#, kde-format
msgid "Use compact list item style"
msgstr "Usa un estil compacte d'elements de llista"

#: package/contents/ui/ConfigGeneral.qml:159
#, kde-format
msgctxt "@info:usagetip under a checkbox when Touch Mode is on"
msgid "Automatically disabled when in Touch Mode"
msgstr "Desactivat automàticament en el mode tàctil"

#: package/contents/ui/ConfigGeneral.qml:168
#, kde-format
msgctxt "@action:button"
msgid "Configure Enabled Search Plugins…"
msgstr "Configura els connectors de cerca habilitats…"

#: package/contents/ui/ConfigGeneral.qml:178
#, kde-format
msgid "Show favorites:"
msgstr "Mostra els preferits:"

#: package/contents/ui/ConfigGeneral.qml:179
#, kde-format
msgctxt "Part of a sentence: 'Show favorites in a grid'"
msgid "In a grid"
msgstr "En una quadrícula"

#: package/contents/ui/ConfigGeneral.qml:187
#, kde-format
msgctxt "Part of a sentence: 'Show favorites in a list'"
msgid "In a list"
msgstr "En una llista"

#: package/contents/ui/ConfigGeneral.qml:195
#, kde-format
msgid "Show other applications:"
msgstr "Mostra altres aplicacions:"

#: package/contents/ui/ConfigGeneral.qml:196
#, kde-format
msgctxt "Part of a sentence: 'Show other applications in a grid'"
msgid "In a grid"
msgstr "En una quadrícula"

#: package/contents/ui/ConfigGeneral.qml:204
#, kde-format
msgctxt "Part of a sentence: 'Show other applications in a list'"
msgid "In a list"
msgstr "En una llista"

#: package/contents/ui/ConfigGeneral.qml:216
#, kde-format
msgid "Show buttons for:"
msgstr "Mostra els botons per a:"

#: package/contents/ui/ConfigGeneral.qml:217
#: package/contents/ui/LeaveButtons.qml:115
#, kde-format
msgid "Power"
msgstr "Aturada"

#: package/contents/ui/ConfigGeneral.qml:226
#, kde-format
msgid "Session"
msgstr "Sessió"

#: package/contents/ui/ConfigGeneral.qml:235
#, kde-format
msgid "Power and session"
msgstr "Aturada i sessió"

#: package/contents/ui/ConfigGeneral.qml:244
#, kde-format
msgid "Show action button captions"
msgstr "Mostra les llegendes dels botons d'acció"

#: package/contents/ui/Footer.qml:97
#, kde-format
msgid "Applications"
msgstr "Aplicacions"

#: package/contents/ui/Footer.qml:110
#, kde-format
msgid "Places"
msgstr "Llocs"

#: package/contents/ui/FullRepresentation.qml:119
#, kde-format
msgctxt "@info:status"
msgid "No matches"
msgstr "Sense coincidències"

#: package/contents/ui/Header.qml:64
#, kde-format
msgid "Open user settings"
msgstr "Obre la configuració de l'usuari"

#: package/contents/ui/Header.qml:240
#, kde-format
msgid "Keep Open"
msgstr "Mantén oberta"

#: package/contents/ui/Kickoff.qml:303
#, kde-format
msgid "Edit Applications…"
msgstr "Edita les aplicacions…"

#: package/contents/ui/KickoffGridView.qml:88
#, kde-format
msgid "Grid with %1 rows, %2 columns"
msgstr "Quadrícula amb %1 files, %2 columnes"

#: package/contents/ui/LeaveButtons.qml:115
#, kde-format
msgid "Leave"
msgstr "Surt"

#: package/contents/ui/LeaveButtons.qml:115
#, kde-format
msgid "More"
msgstr "Més"

#: package/contents/ui/PlacesPage.qml:50
#, kde-format
msgctxt "category in Places sidebar"
msgid "Computer"
msgstr "Ordinador"

#: package/contents/ui/PlacesPage.qml:51
#, kde-format
msgctxt "category in Places sidebar"
msgid "History"
msgstr "Historial"

#: package/contents/ui/PlacesPage.qml:52
#, kde-format
msgctxt "category in Places sidebar"
msgid "Frequently Used"
msgstr "Usats amb freqüència"
