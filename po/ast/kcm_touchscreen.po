# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Enol P. <enolp@softastur.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-05 02:15+0000\n"
"PO-Revision-Date: 2023-05-03 21:44+0200\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: \n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.0\n"

#: kcmtouchscreen.cpp:44
#, kde-format
msgid "Automatic"
msgstr ""

#: kcmtouchscreen.cpp:50
#, kde-format
msgctxt "model - (x,y widthxheight)"
msgid "%1 - (%2,%3 %4×%5)"
msgstr ""

#: ui/main.qml:29
#, kde-format
msgid "No touchscreens found"
msgstr ""

#: ui/main.qml:45
#, kde-format
msgctxt "@label:listbox The device we are configuring"
msgid "Device:"
msgstr "Preséu:"

#: ui/main.qml:58
#, kde-format
msgid "Enabled:"
msgstr ""

#: ui/main.qml:66
#, kde-format
msgid "Target display:"
msgstr ""
